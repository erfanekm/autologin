
# to send login and logout requests
import requests

# used for login or logout
def login(loginPageAddress: str, post: dict) -> requests.models.Response:
    requests.post(loginPageAddress, post)
