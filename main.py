from autologin import login

# address to send login request post
login_address = 'http://example.com/login'

# address to send logout request post
logout_address = 'http://example.com/logout'

# username for logging in
username = 'usrnme'

# ppassword for logging in
password = 'passwd'

# input field name for inserting username
usernameInput = 'username'

# input field name for inserting password
passwordInput = 'password'

# submit button name for login
loginSubmitInput = 'login'

# submit button name for logout
logoutSubmitInput = 'logout'

# post to pass to login function when logging in
loginPost = {
    usernameInput:username,
    passwordInput:password,
    loginSubmitInput:'submit'
}

# post to pass to logout function when logging out
logoutPost = {
    logoutSubmitInput:'submit'
}

# automatically login and logout once
login(login_address, loginPost)
login(logout_address, logoutPost)
